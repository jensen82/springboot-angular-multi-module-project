package sample.multimodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the class that is to be run and belongs to the Application module
 * that is the main module of the project.
 *
 * It contains the application class in which the main method is defined that is necessary to
 * run the Spring Boot Application. It also contains application configuration properties,
 * Controller, views, and resources.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@SpringBootApplication
public class SampleWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleWebApplication.class, args);
    }
}
